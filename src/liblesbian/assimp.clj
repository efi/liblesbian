(ns liblesbian.assimp
  (:require [clojure.pprint :refer [pprint]]
            [clojure.java.io :as io]
            [clojure.core.matrix :as m])
  (:import  (org.lwjgl.assimp Assimp)
            (org.lwjgl BufferUtils)
            (org.joml Matrix4f)))

(defn- log [o]
  (pprint o)
  o)

(defn puke [n o]
  (spit (str n) (with-out-str (pprint o))))

(def asset-manager (atom {:scenes {}}))

(declare visited?)

(defn p-addr [o]
  (if (instance? org.lwjgl.system.Pointer o)
    (.address o)
    (if (number? o)
      o
      (do (println (str "Wrong pointer value? " o)) o))))

(defn buffer->vec [buffer]
  (let [result-fn (-> (class buffer)
         {java.nio.DirectByteBuffer            #(->> % .limit byte-array)
          java.nio.ByteBuffer                  #(->> % .limit byte-array)
          java.nio.IntBuffer                   #(->> % .limit int-array)
          java.nio.DirectIntBufferU            #(->> % .limit int-array)
          java.nio.LongBuffer                  #(->> % .limit long-array)
          java.nio.CharBuffer                  #(->> % .limit char-array)
          java.nio.FloatBuffer                 #(->> % .limit float-array)
          org.lwjgl.PointerBuffer              #(->> % .limit long-array)
          org.lwjgl.assimp.AIVector3D$Buffer   #(->> % .iterator iterator-seq vec)
          org.lwjgl.assimp.AIFace$Buffer       #(->> % .iterator iterator-seq vec)})
        result (result-fn buffer)]
    (if-not (vector? result)
      (do (.get buffer result)
          (vec result))
      result)))

(defn buffer->str [buffer]
  (String. (byte-array (buffer->vec buffer))))

(defn transform [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIMatrix4x4/createSafe)]
   {:object obj
    :matrix (Matrix4f. (.a1 obj) (.b1 obj) (.c1 obj) (.d1 obj)
                       (.a2 obj) (.b2 obj) (.c2 obj) (.d2 obj) 
                       (.a3 obj) (.b3 obj) (.c3 obj) (.d3 obj)
                       (.a4 obj) (.b4 obj) (.c4 obj) (.d4 obj))}))

(defn vec2 [pointer]
  (when-let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIVector2D/createSafe)]
    {:object obj
     :x (.x obj)
     :y (.y obj)}))

(defn vec3 [pointer]
  (when-let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIVector3D/createSafe)]
    {:object obj
     :x (.x obj)
     :y (.y obj)
     :z (.z obj)}))

(defn color [pointer]
  (when-let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIColor4D/createSafe)]
    {:object obj
     :r (.r obj)
     :g (.g obj)
     :b (.b obj)
     :a (.a obj)}))

(defn face [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIFace/createSafe)]
    {:object  obj
     :indices (some->> obj .mIndices buffer->vec)}))

(defn weight [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIVertexWeight/createSafe)]
    {:object  obj
     :id      (some->> obj .mVertexId)
     :weight  (some->> obj .mWeight)}))

(defn bone [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIBone/createSafe)]
    {:object        obj
     :name          (some->> obj .mName .data buffer->str)
     :weights       (some->> obj .mWeights buffer->vec (mapv weight))
     :offsetmatrix  (some->> obj .mOffsetMatrix transform)}))

(defn property [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIMaterialProperty/createSafe)]
    {:object    obj
     :key       (some->> obj .mKey .data buffer->str)
     :semantic  (some->> obj .mSemantic)
     :index     (some->> obj .mIndex)
     :type      (some->> obj .mType)
     :data      (some->> obj .mData)}))

(defn material [pointer]
  (when-not (visited? (p-addr pointer))
    (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIMaterial/createSafe)]
      {:object      obj
       :properties  (some->> obj .mProperties buffer->vec (mapv property))})))

(defn mesh [pointer]
  (when-not (visited? (p-addr pointer))
    (when-let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIMesh/createSafe)]
      {:object          obj
       :name            (some->> obj .mName .data buffer->str)
       :primitivetypes  (some->> obj .mPrimitiveTypes)
       :vertices        (some->> obj .mVertices buffer->vec (mapv vec3))
       :normals         (some->> obj .mNormals buffer->vec (mapv vec3))
       :tangents        (some->> obj .mTangents buffer->vec (mapv vec3))
       :bitangent       (some->> obj .mBitangents buffer->vec (mapv vec3))
       :colors          (some->> obj .mColors buffer->vec (mapv color))
       :texturecoords   (some->> obj .mTextureCoords buffer->vec (mapv vec3))
       :faces           (some->> obj .mFaces buffer->vec (mapv face))
       :bones           (some->> obj .mBones buffer->vec (mapv bone))
       :material        (some->> obj .mMaterialIndex)})))

(defn meta-entry [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIMetaDataEntry/createSafe)]
    {:object  obj
     :type    (some->> obj .mType)
     :data    (some->> obj .mData buffer->vec)}))

(defn metadata [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIMetaData/createSafe)]
    {:object  obj
     :keys    (some->> obj .mKeys buffer->vec (mapv #(->> % .data buffer->str)))
     :values  (some->> obj .mValues buffer->vec (mapv meta-entry))}))

(defn node [pointer]
  (when-not (visited? (p-addr pointer))
    (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AINode/createSafe)]
      {:object    obj
       :node-id   (p-addr pointer)
       :name      (some->> obj .mName .data buffer->str)
       :transform (some->> obj .mTransformation transform)
       :parent    (some->> obj .mParent p-addr)
       :children  (some->> obj .mChildren buffer->vec (mapv p-addr))
       :meshes    (some->> obj .mMeshes buffer->vec)
       :metadata  (some->> obj .mMetadata metadata)})))

(defn quaternion [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIQuaternion/createSafe)]
    {:object obj
     :x (.x obj)
     :y (.y obj)
     :z (.z obj)
     :w (.w obj)}))

(defn node-anim [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AINodeAnim/createSafe)]
    {:object obj
     :name        (some->> obj .mName .data buffer->str)
     :positions   (some->> obj .mPositionKeys buffer->vec (mapv vec3))
     :rotations   (some->> obj .mRotationKeys buffer->vec (mapv quaternion))
     :scales      (some->> obj .mScalingKeys buffer->vec (mapv vec3))
     :state-pre   (some->> obj .mPreState)
     :state-post  (some->> obj .mPostState)}))

(defn mesh-anim [])

(defn animation [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AIAnimation/createSafe)]
    {:object        obj
     :name          (some->> obj .mName .data buffer->str)
     :duration      (some->> obj .mDuration)
     :speed         (some->> obj .mTicksPerSecond)
     :channels      (some->> obj .mChannels buffer->vec (mapv node-anim))
     :meshchannels  (some->> obj .mMeshChannels buffer->vec (mapv mesh-anim))}))

(defn texture [pointer]
  (when-not (visited? (p-addr pointer))
    (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AITexture/createSafe)]
      {:object    obj
       :width     (some->> obj .mWidth)
       :height    (some->> obj .mHeight)
       :format    (some->> obj .mFormatHint)
       :data      (some->> obj .pcData)
       :filename  (some->> obj .mFilename .data buffer->str)})))

(defn light [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AILight/createSafe)]
    {:object                obj
     :name                  (some->> obj .mName .data buffer->str)
     :type                  (some->> obj .mType)
     :position              (some->> obj .mPosition vec3)
     :direction             (some->> obj .mDirection vec3)
     :up                    (some->> obj .mUp vec3)
     :attenuation-constant  (some->> obj .mAttenuationConstant)
     :attenuation-linear    (some->> obj .mAttenuationLinear)
     :attenuation-quadratic (some->> obj .mAttenuationQuadratic)
     :color-diffuse         (some->> obj .mColorDiffuse color)
     :color-specular        (some->> obj .mColorSpecular color)
     :color-ambient         (some->> obj .mColorAmbient color)
     :angle-inner           (some->> obj .mAngleInnerCone)
     :angle-outer           (some->> obj .mAngleOuterCone)
     :size                  (some->> obj .mSize vec2)}))

(defn camera [pointer]
  (let [obj (-> (p-addr pointer) org.lwjgl.assimp.AICamera/createSafe)]
    {:object    obj
     :name      (some->> obj .mName .data buffer->str)
     :position  (some->> obj .mPosition vec3)
     :up        (some->> obj .mUp vec3)
     :look-at   (some->> obj .mLookAt vec3)
     :fov       (some->> obj .mHorizontalFOV)
     :near      (some->> obj .mClipPlaneNear)
     :far       (some->> obj .mClipPlaneFar)
     :aspect    (some->> obj .mAspect)}))

(defn scene [sc]
  {:object      sc
   :flags       (some->> sc .mFlags)
   :root        (some->> sc .mRootNode p-addr)
   :meshes      (some->> sc .mMeshes buffer->vec (mapv mesh))
   :materials   (some->> sc .mMaterials buffer->vec (mapv material))
   :animations  (some->> sc .mAnimations buffer->vec (mapv animation))
   :textures    (some->> sc .mTextures buffer->vec (mapv texture))
   :lights      (some->> sc .mLights buffer->vec (mapv light))
   :cameras     (some->> sc .mCameras buffer->vec (mapv camera))
   ;:metadata    (some->> sc .mMetaData metadata)
   })

(def ^:dynamic visited)

(defn collect-nodes [scene]
  (assoc scene :nodes
         (loop [node-ids [(:root scene)]
                nodes {}]
           (let [node-id (first node-ids)]
             (if (nil? node-id)
               nodes
               (if (contains? nodes node-id)
                 (recur (next node-ids) nodes)
                 (let [node-obj (node node-id)]
                   (recur (concat (next node-ids) (:children node-obj))
                          (assoc nodes node-id node-obj)))))))))

(defn node-by-name [nodes n]
  (first (filter #(= (:name %) n) (vals nodes))))

(defn camera-to-view-matrix [position target up]
  (let [position (m/matrix [(:x position) (:y position) (:z position)])
        target (m/matrix [(:x target) (:y target) (:z target)]) 
        up (m/matrix [(:x up) (:y up) (:z up)]) 
        z-axis (m/normalise (m/sub position target))
        x-axis (m/normalise (m/cross up z-axis))
        y-axis (m/cross z-axis x-axis)
        x #(m/mget % 0)
        y #(m/mget % 1)
        z #(m/mget % 2)]
    (m/matrix
      [[(x x-axis) (x y-axis) (x z-axis) 0]
       [(y x-axis) (y y-axis) (y z-axis) 0]
       [(z x-axis) (z y-axis) (z z-axis) 0]
       [(- (m/dot x-axis position)) (- (m/dot y-axis position)) (- (m/dot z-axis position)) 1]])))

(defn fov->lrtb [cam]
  ;form assimp, fov is already half horizontal angle in radians
  (let [aspect (if (= 0.0 (:aspect cam)) (/ 4 3) (:aspect cam))
        tangent (/ 1 (Math/tan (* 0.5 (:fov cam))))
        w (* tangent (:near cam))
        h (* aspect w)]
    [(- w) w (- h) h (:near cam) (:far cam)]))

(defn camera-to-proj-matrix [cam]
  (let [a (if (= 0.0 (:aspect cam)) (/ 4 3) (:aspect cam))
        h (Math/tan (:fov cam))
        zm (- (:near cam) (:far cam))
        zp (+ (:near cam) (:far cam))]
    (m/matrix
      [[(/ 1 (* h a))   0         0           0                                   ]
       [0               (/ 1 h)   0           0                                   ]
       [0               0         (/ zp zm)   (/ (* 2 (:far cam) (:near cam)) zm) ]
       [0               0         -1          0                                   ]])))

(def default-camera {})
(defn bake-scene [scene]
  (let [nodes (:nodes scene)
        cameras (if (empty? (:cameras scene))
                  default-camera
                  (for [cam (:cameras scene)
                        :let [cam-node (node-by-name nodes (:name cam))]]
                    (assoc cam :transform (:transform cam-node))))
        camera (first cameras)
        view-matrix (-> camera :transform :matrix)
        proj-matrix (-> (Matrix4f.)
                        (.setPerspective (* 0.5 (:fov camera))
                                         (if (= 0.0 (:aspect camera)) 1.333 (:aspect camera))
                                         (:near camera)
                                         (:far camera))
                        ;(.invertPerspective)
                        )
        lights (when (> (count (:lights scene)) 0)
                 (for [l (:lights scene)
                       :let [lnode (node-by-name nodes (:name l))]]
                   (assoc l :transform (:transform lnode))))
        baked-scene {:nodes nodes
                     :cameras cameras
                     :camera camera
                     :view-matrix view-matrix
                     :proj-matrix proj-matrix
                     :lights lights}
        geometry (remove #(nil? (:meshes (val %))) (:nodes baked-scene))
        parent-node (fn [node] (get (:nodes baked-scene) (:parent node)))
        final-transform (fn [fnode]
                          (loop [node fnode
                                 trans (Matrix4f. (-> node :transform :matrix))]
                            (if (nil? (:parent node))
                              trans
                              (recur (parent-node node) (.mul trans (-> node parent-node :transform :matrix))))))]
    (assoc baked-scene
           :geometry geometry
           :final-transform final-transform)))

(defn scene-import [& filenames]
  (let [fname (.toString (.getPath (java.nio.file.FileSystems/getDefault) "resources" (into-array filenames)))
        f (io/file fname)]
    (when (and (.exists f)
               (.isFile f))
      (binding [visited (atom [])]
        (collect-nodes
          (scene (Assimp/aiImportFile
                   fname
                   (bit-or Assimp/aiProcess_OptimizeMeshes
                           Assimp/aiProcess_Triangulate
                           Assimp/aiProcess_JoinIdenticalVertices))))))))

(defn visited? [n]
  (if (contains? @visited n)
    true
    (do (swap! visited conj n)
        false)))

(defn mesh-cache [meshes]
  (->> meshes
       (map :vertices)
       (map-indexed hash-map)
       (apply merge)
       (map (fn [m]
              (hash-map (key m)
                        (-> (map (fn [v]
                                   (list (:x v) (:y v) (:z v)))
                                 (val m))
                            flatten
                            float-array))))
       (apply merge)))

(defn index-cache [meshes]
  (->> meshes
       (map :faces)
       (map-indexed hash-map)
       (apply merge)
       (map (fn [m]
              (hash-map (key m)
                        (-> (map :indices
                                 (val m))
                            flatten
                            int-array))))
       (apply merge)))

(defn normals-cache [meshes]
  (->> meshes
       (map :normals)
       (map-indexed hash-map)
       (apply merge)
       (map (fn [m]
              (hash-map (key m)
                        (-> (map (fn [v]
                                   (list (:x v) (:y v) (:z v)))
                                 (val m))
                            flatten
                            float-array))))
       (apply merge)))

(defn load-scene [filename]
  (swap! asset-manager assoc-in [:scenes filename] (scene-import filename))
  (let [meshes (get-in @asset-manager [:scenes filename :meshes])]
    (swap! asset-manager
           #(-> %
                (assoc-in [:mesh-cache filename] (mesh-cache meshes))
                (assoc-in [:index-cache filename] (index-cache meshes))
                (assoc-in [:normals-cache filename] (normals-cache meshes))
                (update-in [:scenes filename] bake-scene))))
  (let [r (get-in @asset-manager [:scenes filename])]
    (puke "last-scene-structure" r)
    r))

(defn node->mesh
  ([scene-name node-item]
    (node->mesh scene-name node-item 0))
  ([scene-name node-item n]
    (get-in @asset-manager [:mesh-cache scene-name (get-in node-item [:meshes n])])))

