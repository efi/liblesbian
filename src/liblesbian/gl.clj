(ns liblesbian.gl
  (:require [clojure.core.matrix :refer :all]) 
  (:import (org.lwjgl.opengl GL GL40C)
           (org.lwjgl.system MemoryStack)))

; Map every static constant from the GL* enums to unprefixed variables for ease of use
(doall
  (map
    (fn [field]
      (let [fname (.getName field)]
        (intern *ns*
                (->> fname (re-find #"GL_(.*)") second symbol)
                (eval (list '. GL40C (symbol fname))))))
    (.getFields GL40C)))


; oops, matrix math has a log function for logarithms
(defn- errlog [o]
  (clojure.pprint/pprint o)
  o)

(declare color-hex)

(defn color
  ([kw]
    (if-not (keyword? kw)
      (color-hex kw)
      (case kw
        :black  (color 0 0 0)
        :white  (color 1 1 1)
        :red    (color 1 0 0)
        :green  (color 0 1 0)
        :blue   (color 0 0 1)
        :yellow (color 1 1 0)
        :cyan   (color 0 1 1)
        :purple (color 1 0 1)
        (do (errlog "Unknown color keyword")
            (color 0 0 0)))))
  ([r g b] (color r g b 1))
  ([r g b a] {:r r :g g :b b :a a}))

(def color-hex
  (memoize
    (fn [hex]
      (cond 
        (string? hex)
          (or (when-let [hex (last (re-matches #"(#|0x)?(\p{XDigit}{6}|\p{XDigit}{8})" hex))]
                (->> hex
                     (partition 2)
                     (map #(-> %
                               join
                               (Integer/parseInt 16)
                               (/ 0xff)))
                     (apply color)))
              (do (errlog (format "Error parsing color: %s" hex)) (color 0 0 0)))
        (number? hex)
          (cond
            (< hex 0)
              (do (errlog "Negative color") (color 0 0 0))
            (> hex 0xffffffff)
              (do (errlog "Color number too large") (color 1 1 1))
            :else
              (let [n (if (<= hex 0xffffff) 3 4)]
                (->> hex
                     (iterate #(bit-shift-right % 8))
                     (take n)
                     (mapv #(/ (bit-and % 0xff) 255))
                     rseq
                     (apply color))))
        :else (do (errlog "Couldn't create color") (color 0 0 0))))))

(set-current-implementation :vectorz)

(defn new-matrix-4x4
  ([] (identity-matrix 4))
  ([a b c d]
    (when-not (and (coll? a) (coll? b) (coll? c) (coll? d)
                   (= 4 (count a))
                   (= 4 (count b))
                   (= 4 (count c))
                   (= 4 (count d))
                   (every? number? a)
                   (every? number? b)
                   (every? number? c)
                   (every? number? d))
      (errlog "Wrong matrix values"))
    (matrix [a b c d]))
  ([a1 a2 a3 a4
    b1 b2 b3 b4
    c1 c2 c3 c4
    d1 d2 d3 d4]
    (when-not (every? number? [a1 a2 a3 a4 b1 b2 b3 b4 c1 c2 c3 c4 d1 d2 d3 d4])
      (errlog "Wrong matrix values"))
    (matrix [[a1 a2 a3 a4]
                 [b1 b2 b3 b4]
                 [c1 c2 c3 c4]
                 [d1 d2 d3 d4]])))

(defn quaternion
  ([] (quaternion 0 0 0 0))
  ([v]
    (when-not (and (coll? v) (= 4 (count v)) (every? number? v))
      (log "Wrong quaternion values"))
    (apply quaternion v))
  ([i j k w]
    (when-not (every? number? [i j k w])
      (errlog "Wrong quaternion values"))
    (matrix [i j k w])))

(defn clear-color [c]
  (GL40C/glClearColor
    (float (:r c))
    (float (:g c))
    (float (:b c))
    (float (:a c))))

(defn clear [bf]
  (GL40C/glClear bf))

(defn create-capabilities []
  (GL/createCapabilities))

(defn viewport [x y w h]
  (GL40C/glViewport x y w h))

(defn polygon-mode [side fill]
  (GL40C/glPolygonMode side fill))

(defn enable [capability]
  (GL40C/glEnable capability))

(defn vertex-attrib-pointer [attrib size kind normalize stride offset]
  (GL40C/glVertexAttribPointer (int attrib) (int size) (int kind) (boolean normalize) (int stride) (long offset)))

(defn enable-vertex-attrib-array [attrib]
  (GL40C/glEnableVertexAttribArray (int attrib)))

(defn VBO []
  (let [b {:id (GL40C/glGenBuffers)}]
    (merge b {:bind #(GL40C/glBindBuffer ARRAY_BUFFER (:id b))
              :set-data #(GL40C/glBufferData ARRAY_BUFFER % STATIC_DRAW)})))

(defn EBO []
  (let [b {:id (GL40C/glGenBuffers)}]
    (merge b {:bind #(GL40C/glBindBuffer ELEMENT_ARRAY_BUFFER (:id b))
              :set-data #(GL40C/glBufferData ELEMENT_ARRAY_BUFFER % STATIC_DRAW)})))

(defn VAO []
  (let [b {:id (GL40C/glGenVertexArrays)}]
    (merge b {:bind #(GL40C/glBindVertexArray (:id b))})))

(defn shader [kind] ; kind is VERTEX_SHADER FRAGMENT_SHADER etc
  (let [s {:id (GL40C/glCreateShader kind)}]
    (merge s {:set-source #(GL40C/glShaderSource (:id s) (str %))
              :compile    #(GL40C/glCompileShader (:id s))
              :delete     #(GL40C/glDeleteShader (:id s))
              :log        #(errlog (GL40C/glGetShaderInfoLog (:id s)))})))

(defn get-attr [id s]
  (let [a {:id (GL40C/glGetAttribLocation id s)}]
    (merge a {:enable #(GL40C/glEnableVertexAttribArray (int (:id a)))
              :vap    #(GL40C/glVertexAttribPointer
                         (int (:id a))
                         (int %)
                         (int FLOAT)
                         (boolean FALSE)
                         (int 0)
                         (long 0))})))

(defn get-program-iv [p kindp]
  (for [u (range (GL40C/glGetProgrami p kindp))]
    (let [stack (MemoryStack/stackPush)
          size (.mallocInt stack (int 1))
          kind (.mallocInt stack (int 1))
          uname (condp = kindp
                  ACTIVE_UNIFORMS (GL40C/glGetActiveUniform p u size kind)
                  ACTIVE_ATTRIBUTES (GL40C/glGetActiveAttrib p u size kind))]
      (MemoryStack/stackPop)
      {:id u :name uname :size (.get size 0) :kind (.get kind 0)})))

(defn get-buffer-parameter [target pname]
  (GL40C/glGetBufferParameteri (int target) (int pname)))

(defn get-buffer-size [target]
  (/ (get-buffer-parameter target BUFFER_SIZE) 4))

(defn shader-program []
  (let [p {:id (GL40C/glCreateProgram)}]
    (merge p {:attach         #(GL40C/glAttachShader (:id p) (int (:id %)))
              :detach         #(GL40C/glDetachShader (:id p) (int (:id %)))
              :attr           #(get-attr (:id p) %)
              :link           #(GL40C/glLinkProgram (:id p))
              :use            #(GL40C/glUseProgram (:id p))
              :validate       #(GL40C/glValidateProgram (:id p))
              :get-uniforms   #(get-program-iv (:id p) ACTIVE_UNIFORMS)
              :get-attributes #(get-program-iv (:id p) ACTIVE_ATTRIBUTES)
              :delete         #(GL40C/glDeleteProgram (:id p))
              :log            #(errlog (GL40C/glGetProgramInfoLog (:id p)))})))

(declare shader-inputs)
(defn gather-variables [sp]
  (def shader-inputs
    (errlog{:uniforms (vec ((:get-uniforms sp)))
     :attributes (vec ((:get-attributes sp)))})))

(defn uniform-by-name [uname]
  (when shader-inputs
    (->> shader-inputs
         :uniforms
         (filter #(= (:name %) uname))
         first)))

(defn upload-uniform [uniform-name data]
  (let [u (uniform-by-name uniform-name)
        id (int (:id u))
        data (if (matrix? data)
               (to-vector data)
               data)]
    (condp = (:kind u)
      BOOL              (GL40C/glUniform1ui       id (boolean data))
      BOOL_VEC2         (GL40C/glUniform2uiv      id (boolean-array data))
      BOOL_VEC3         (GL40C/glUniform3uiv      id (boolean-array data))
      BOOL_VEC4         (GL40C/glUniform4uiv      id (boolean-array data))
      INT               (GL40C/glUniform1i        id (int data))
      INT_VEC2          (GL40C/glUniform2iv       id (int-array data))
      INT_VEC3          (GL40C/glUniform3iv       id (int-array data))
      INT_VEC4          (GL40C/glUniform4iv       id (int-array data))
      FLOAT             (GL40C/glUniform1f        id (float data))
      FLOAT_VEC2        (GL40C/glUniform2fv       id (float-array data))
      FLOAT_VEC3        (GL40C/glUniform3fv       id (float-array data))
      FLOAT_VEC4        (GL40C/glUniform4fv       id (float-array data))
      FLOAT_MAT2        (GL40C/glUniformMatrix2fv id (boolean false) (float-array data))
      FLOAT_MAT3        (GL40C/glUniformMatrix3fv id (boolean false) (float-array data))
      FLOAT_MAT4        (GL40C/glUniformMatrix4fv id (boolean false) (float-array data))
      SAMPLER_1D        (GL40C/glUniform1i        id (int data))
      SAMPLER_1D_SHADOW (GL40C/glUniform1i        id (int data))
      SAMPLER_2D        (GL40C/glUniform1i        id (int data))
      SAMPLER_2D_SHADOW (GL40C/glUniform1i        id (int data))
      SAMPLER_3D        (GL40C/glUniform1i        id (int data))
      SAMPLER_CUBE      (GL40C/glUniform1i        id (int data)))))

(defn draw []
  #_(GL40C/glDrawArrays TRIANGLES 0 (get-buffer-size ARRAY_BUFFER))
  (GL40C/glDrawElements TRIANGLES (get-buffer-size ELEMENT_ARRAY_BUFFER) UNSIGNED_INT 0))
