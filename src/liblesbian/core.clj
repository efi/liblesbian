(ns liblesbian.core
  (:require [liblesbian.glfw :as glfw]
            [liblesbian.gl :as gl]
            [liblesbian.assimp :as ass]
            [tile-soup.core :as Tiled]
            [clojure.core.matrix :as mat]
            [clojure.string :refer [join]]
            [clojure.pprint :refer [pprint]])
  (:import (org.joml Matrix4f))
  (:gen-class))

(defn- log [o]
  (pprint o)
  o)

(def p println)

(def asset-manager ass/asset-manager)

(def scene-file "monkey.dae")
(defn read-file [f]
  (slurp (.toString (.getPath (java.nio.file.FileSystems/getDefault) "resources" (into-array [f])))))

(defn find-kv-by-key [kv l k v]
  (kv (first (filter #(= (k %) v) l))))

(defn shader-input [n]
  (find-kv-by-key :id (:attributes gl/shader-inputs) :name n))

(defn load-shaders []
  (let [vs (gl/shader gl/VERTEX_SHADER)
        fs (gl/shader gl/FRAGMENT_SHADER)
        sp (gl/shader-program)]
    ((:set-source vs) (read-file "vertex_shader"))
    ((:set-source fs) (read-file "fragment_shader"))
    ((:compile vs))
    ((:compile fs))
    ((:log vs))
    ((:log fs))
    ((:attach sp) vs)
    ((:attach sp) fs)
    ((:link sp))
    ((:validate sp))
    ((:use sp))
    ((:log sp))
    (gl/gather-variables sp)
    sp))

(defn -init []
  (.set (org.lwjgl.glfw.GLFWErrorCallback/createPrint System/err))
  (when-not (glfw/init)
    (throw (IllegalStateException. "Unable to initialize GLFW")))
  (glfw/default-window-hints)
  (glfw/window-hint glfw/VISIBLE glfw/FALSE)
  (glfw/window-hint glfw/RESIZABLE glfw/TRUE)
  (let [window (glfw/create-window 800 600 "I'm gay!")]
    (when (nil? window)
      (throw (RuntimeException. "Failed to create the GLFW window")))
    (glfw/set-key-callback
      window
      (when (and (= $key glfw/KEY_ESCAPE)
                 (= $action glfw/RELEASE))
        (log "Closing normally...")
        (glfw/set-window-should-close $window true)))
    (try
      (let [size (glfw/get-window-size window)
            vidmode (glfw/get-video-mode (glfw/get-primary-monitor))]
        (glfw/set-window-pos
          window
          (/ (- (.width vidmode) (first size)) 2)
          (/ (- (.height vidmode) (second size)) 2))))
    (glfw/make-context-current window)
    (gl/create-capabilities)
    (def sp (load-shaders))
    (glfw/swap-interval 1)
    (glfw/show-window window)
    (gl/viewport 0 0 800 600)
    (gl/enable gl/CULL_FACE)
    (gl/enable gl/DEPTH_TEST)
    ;(gl/polygon-mode gl/FRONT_AND_BACK gl/FILL)
    (doseq [f [scene-file]]
      (ass/load-scene f))
    (def vaos [(gl/VAO)])
    (def vbos [(gl/VBO) (gl/VBO)])
    (def ebos [(gl/EBO)])
    (let [manager @asset-manager
          mesh (get-in manager [:mesh-cache scene-file 0])
          indices (get-in manager [:index-cache scene-file 0])
          normals (get-in manager [:normals-cache scene-file 0])
          pos ((:attr sp) "position")
          nor ((:attr sp) "normal")]
      ((:bind (first vaos)))
      ((:bind (first ebos)))
      ((:set-data (first ebos)) indices)
      ((:bind (first vbos)))
      ((:set-data (first vbos)) mesh)
      ((:enable pos))
      ((:vap pos) 3)
      ((:bind (second vbos)))
      ((:set-data (second vbos)) normals)
      ((:enable nor))
      ((:vap nor) 3))
    window))

(defn render [n] (log n))

(defn matrix->float-array [m]
  (float-array [(.m00 m) (.m01 m) (.m02 m) (.m03 m)
                (.m10 m) (.m11 m) (.m12 m) (.m13 m)
                (.m20 m) (.m21 m) (.m22 m) (.m23 m)
                (.m30 m) (.m31 m) (.m32 m) (.m33 m)]))

(defn matrix->xyz [m]
  [(.m30 m) (.m31 m) (.m32 m)])

(defn handle-keys [G] G)
(defn handle-collission [G] G)
(defn draw-all [G]
  ;(gl/draw)
  (let [scene (:scene G)
;        monkey (ass/node-by-name (:nodes scene) "Suzanne")
        omr (:original-monkey-rotation G)
        cmr (:current-monkey-rotation G)
        ovm (:view-matrix G)
        cvm (:cvm G)
        opj (:opj G)
        cpj (:cpj G)
        [hp vp] (map #(* 0.01 %) (glfw/get-cursor-pos (:window G))) ;cursor position
        ]
    (.set cmr omr)
    ;(.translate cmr (float vp) (float hp) (float 0))
    ;(.rotateXYZ cmr (float vp) (float hp) (float 0))
    (.set cvm ovm)
    (.invert cvm)
    ;(.rotateLocalX cvm (float vp))
    ;(.rotateLocalY cvm (float hp))
    (.set cpj opj)
    ;(gl/upload-uniform "model" ((:final-transform scene) (first (:geometry scene))))
    (let [camt (-> G :camera :transform :matrix matrix->xyz)]
      (gl/upload-uniform "viewPos" (float-array camt)))
    (let [lightt (-> scene :lights first ((:final-transform scene)) matrix->xyz)]
      (gl/upload-uniform "lightPos" (float-array lightt)))
    (gl/upload-uniform "model" (matrix->float-array cmr))
    ;(gl/upload-uniform "view" (matrix->float-array (:view-matrix scene)))
    (gl/upload-uniform "view" (matrix->float-array cvm))
    ;(gl/upload-uniform "proj" (:proj-matrix scene))
    ;(gl/upload-uniform "proj" (matrix->float-array (:proj-matrix scene)))
    (gl/upload-uniform "proj" (matrix->float-array cpj))
    (gl/draw)
    (assoc G :current-monkey-rotation cmr)))
#_(defn draw-all [G]
  (let [manager @asset-manager]
    (loop [scenes (get-in manager [:scenes])]
      (let [sname (first (keys scenes))
            scene (first (vals scenes))]
        (if-not (nil? scene)
          (loop [nodes (remove #(nil? (:meshes %))
                               (tree-seq #(contains? % :children)
                                         #(:children %)
                                         (:root scene)))]
            (let [node (first nodes)]
              (if-not (nil? node)
                (render (ass/node->mesh sname node))
                (when (next nodes)
                  (recur (next nodes))))))
          (when (next scenes)
            (recur (next scenes)))))))
  G)

(def game-step
  (comp draw-all
        handle-collission
        handle-keys))

(defn- new-game [w]
  (let [w-size (glfw/get-framebuffer-size w)
        [wth hth] w-size
        scene (get-in @asset-manager [:scenes scene-file])
        
        ]
    {:time 0
     :bgcolor (gl/color 0x222244)
     :window w
     :w-size w-size
     :scene scene
     :camera (-> scene :camera)
     :view-matrix (Matrix4f. (:view-matrix scene))
     :cvm (Matrix4f.)
     :opj (:proj-matrix scene)
     :cpj (Matrix4f.)
     :original-monkey-rotation (-> scene
                                   :nodes
                                   (ass/node-by-name "Suzanne")
                                   ((:final-transform scene)))
     :current-monkey-rotation (Matrix4f.)
     }))

(def run-this (atom nil))
(def return-data (atom nil))

(defn -loop [window]
  (loop [game (new-game window)]
    (letfn [(now [] (:time game))
            (log [o] (do (p (str "[" (now) "] ")) (pprint o) o))]
      (when (not (glfw/window-should-close window))
        (when (fn? @run-this)
          (try
            (reset! return-data (log (@run-this)))
            (catch Exception ex (log ex))
            (finally (println) (reset! run-this nil))))
        (let [[w h] (glfw/get-framebuffer-size window)]
          (gl/viewport 0 0 w h))
        ;(gl/viewport 0 0 800 600)
        (gl/clear-color (:bgcolor game))
        (glfw/swap-buffers window)
        (gl/clear (bit-or gl/COLOR_BUFFER_BIT gl/DEPTH_BUFFER_BIT))
        (glfw/poll-events)
        (recur (update (game-step game) :time inc)))))
  window)

(defn on-gl [f]
  (reset! run-this f))

(defn -run []
  (try
    (do
      (p (join "\n"
               ["Hello, World! I'm gay!"
                "LWJGL Version:" (org.lwjgl.Version/getVersion)
                "GLFW Version:" (glfw/get-version-string)
                "Clojure libraries:" (join " " (loaded-libs))]))
      (doto ;window
        (-init)
        (-loop)
        (org.lwjgl.glfw.Callbacks/glfwFreeCallbacks)
        (glfw/destroy-window))
      (glfw/terminate)
      (.free (glfw/set-error-callback nil)))
    (catch Exception ex
      (spit "crashlog" (with-out-str (pprint ex))))))

(defn -main
  "Entry point"
  [& args]
  ;(-run)
  ;(p "Closed")
  (let [end (future (-run))]
    (try (while (not (future-done? end)))
         (catch Exception ex (spit "main-crashlog" ex))
         (finally
           (shutdown-agents)
           (p "Closed")))))
