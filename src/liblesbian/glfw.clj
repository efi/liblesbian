(ns liblesbian.glfw
  (:require [clojure.pprint :refer [pprint]])
  (:import (org.lwjgl.glfw GLFW)
           (org.lwjgl.system MemoryStack)
           (java.nio IntBuffer)))

(defn- log [o]
  (pprint o)
  o)

; Map every static constant from the GLFW enums to unprefixed variables for ease of use
; eg.: to call org.lwjgl.glfw/GLFW/GLFW_KEY_A we can now say KEY_A or glfw/KEY_A when requiring this library
(doall
  (map
    (fn [field]
      (let [fname (.getName field)]
        (intern *ns*
                (->> fname (re-find #"GLFW_(.*)") second symbol)
                (eval (list '. GLFW (symbol fname))))))
    (.getFields GLFW)))

(defn anti-void [basefn stackargn kind]     ; take a (new) function name, a method name, the number of normal arguments and the number of arguments that should actually be returned by these functions, but aren't, 'cause C
  (let [stack (MemoryStack/stackPush)                    ; shit that gets modified by the lwjgl C crap
        args (vec (for [n (range stackargn)]          ; make a list of output arguments (!) using the memstack thing
                    (case kind
                      :double (.mallocDouble stack (int 1))
                      :int (.mallocInt stack (int 1))
                      :float (.mallocFloat stack (int 1)))))]
    (apply basefn args)
    (let [ret (mapv #(.get % 0) args)]
      (MemoryStack/stackPop)                               ; bye forever, stack
      ret)))                                             ; return the "output" arguments, now we have a functional way of calling these garbage functions

(defmacro def-callback-macro [fname basefn interface & inner-args]; take a (new) macro name, a method name, the interface it implements and the arguments the callback will take
  (let [args (vec (cons '$this inner-args))]                      ; add $this to the argument list 'cause reify wants it
    `(defmacro ~(symbol fname) ~'[window & callback]              ; make a new macro with the expected arguments
       `(. GLFW ~'~basefn (long ~'~'window)                       ; call the function that sets the callback on the window
           (reify ~'~interface                                    ; reify the callback interface
             (~'~'invoke ~'~args ~@~'callback))))))               ; invoke is the callback method, callback is a literal clojure form that references the arguments (using $variable convention to avoid collissions)

(defn get-library []
  (GLFW/getLibrary))

(defn create-cursor [image x y]
  (GLFW/glfwCreateCursor image (int x) (int y)))

(defn create-standard-cursor [shape]
  (GLFW/glfwCreateStandardCursor (int shape)))

(defn create-window
  ([] (create-window nil nil "Application" nil nil))
  ([width] (create-window width (* width 0.75) "Application" nil nil))
  ([width height] (create-window width height "Application" nil nil))
  ([width height title] (create-window width height title nil nil))
  ([width height title monitor] (create-window width height title monitor nil))
  ([width height title monitor share]
    (GLFW/glfwCreateWindow (int (or width 800))
                           (int (or height 600))
                           (String. title)
                           (long (or monitor 0))
                           (long (or share 0)))))

(defn default-window-hints []
  (GLFW/glfwDefaultWindowHints))

(defn destroy-cursor [cursor]
  (GLFW/glfwDestroyCursor (long cursor)))

(defn destroy-window [window]
  (GLFW/glfwDestroyWindow (long window)))

(defn extension-supported [extension]
  (GLFW/glfwExtensionSupported extension))

(defn focus-window [window]
  (GLFW/glfwFocusWindow (long window)))

(defn get-clipboard-string [window]
  (GLFW/glfwGetClipboardString (long window)))

(defn get-current-context []
  (GLFW/glfwGetCurrentContext))

(defn get-cursor-pos [window]
  (anti-void #(GLFW/glfwGetCursorPos (long window) %1 %2) 2 :double))

(defn get-error [description]
  (GLFW/glfwGetError description))

(defn get-framebuffer-size [window]
  (anti-void #(GLFW/glfwGetFramebufferSize (long window) %1 %2) 2 :int))

(defn get-gamepad-name [jid]
  (GLFW/glfwGetGamepadName (int jid)))

(defn get-gamepad-state [jid state]
  (GLFW/glfwGetGamepadState (int jid) state))

(defn get-gamma-ramp [monitor]
  (GLFW/glfwGetGammaRamp (long monitor)))

(defn get-input-mode [window mode]
  (GLFW/glfwGetInputMode (long window) (int mode)))

(defn get-joystick-axes [jid]
  (GLFW/glfwGetJoystickAxes (int jid)))

(defn get-joystick-buttons [jid]
  (GLFW/glfwGetJoystickButtons (int jid)))

(defn get-joystick-guid [jid]
  (GLFW/glfwGetJoystickGUID (int jid)))

(defn get-joystick-hats [jid]
  (GLFW/glfwGetJoystickHats (int jid)))

(defn get-joystick-name [jid]
  (GLFW/glfwGetJoystickName (int jid)))

(defn get-joystick-user-pointer [jid]
  (GLFW/glfwGetJoystickUserPointer (int jid)))

(defn get-key [window key-]
  (GLFW/glfwGetKey (long window) (int key-)))

(defn get-key-name [key- scancode]
  (GLFW/glfwGetKeyName (int key-) (int scancode)))

(defn get-key-scancode [key-]
  (GLFW/glfwGetKeyScancode (int key-)))

(defn get-monitor-content-scale [window]
  (anti-void #(GLFW/glfwGetMonitorContentScale (long window) %1 %2) 2 :float))

(defn get-monitor-name [monitor]
  (GLFW/glfwGetMonitorName (long monitor)))

(defn get-monitor-physical-size [window]
  (anti-void #(GLFW/glfwGetMonitorPhysicalSize (long window) %1 %2) 2 :int))

(defn get-monitor-pos [window]
  (anti-void #(GLFW/glfwGetMonitorPos (long window) %1 %2) 2 :int))

(defn get-monitor-user-pointer [monitor]
  (GLFW/glfwGetMonitorUserPointer (long monitor)))

; private? undocumented in glfw
;(defn get-monitor-workarea []
;  (GLFW/glfwGetMonitorWorkarea))

(defn get-monitors []
  (GLFW/glfwGetMonitors))

(defn get-mouse-button [window button]
  (GLFW/glfwGetMouseButton (long window) (int button)))

(defn get-primary-monitor []
  (GLFW/glfwGetPrimaryMonitor))

(defn get-proc-address [procname]
  (GLFW/glfwGetProcAddress procname))

(defn get-time []
  (GLFW/glfwGetTime))

(defn get-timer-frequency []
  (GLFW/glfwGetTimerFrequency))

(defn get-timer-value []
  (GLFW/glfwGetTimerValue))

(defn get-version []
  (anti-void #(GLFW/glfwGetVersion %1 %2 %3) 3 :int))

(defn get-version-string []
  (GLFW/glfwGetVersionString))

(defn get-video-mode [monitor]
  (GLFW/glfwGetVideoMode (long monitor)))

(defn get-video-modes [monitor]
  (GLFW/glfwGetVideoModes (long monitor)))

(defn get-window-attrib [window attrib]
  (GLFW/glfwGetWindowAttrib (long window) (int attrib)))

(defn get-window-content-scale [window]
  (anti-void #(GLFW/glfwGetWindowContentScale (long window) %1 %2) 2 :float))

(defn get-window-frame-size [window]
  (anti-void #(GLFW/glfwGetWindowFrameSize (long window) %1 %2 %3 %4) 4 :int))

(defn get-window-monitor [window]
  (GLFW/glfwGetWindowMonitor (long window)))

(defn get-window-opacity [window]
  (GLFW/glfwGetWindowOpacity (long window)))

(defn get-window-pos [window]
  (anti-void #(GLFW/glfwGetWindowPos (long window) %1 %2) 2 :int))

(defn get-window-size [window]
  (anti-void #(GLFW/glfwGetWindowSize (long window) %1 %2) 2 :int))

(defn get-window-user-pointer [window]
  (GLFW/glfwGetWindowUserPointer (long window)))

(defn hide-window [window]
  (GLFW/glfwHideWindow (long window)))

(defn iconify-window [window]
  (GLFW/glfwIconifyWindow (long window)))

(defn init []
  (GLFW/glfwInit))

(defn init-hint [hint value]
  (GLFW/glfwInitHint (int hint) (int value)))

(defn joystick-is-gamepad [jid]
  (GLFW/glfwJoystickIsGamepad (int jid)))

(defn joystick-present [jid]
  (GLFW/glfwJoystickPresent (int jid)))

(defn make-context-current [window]
  (GLFW/glfwMakeContextCurrent (long window)))

(defn maximize-window [window]
  (GLFW/glfwMaximizeWindow (long window)))

(defn poll-events []
  (GLFW/glfwPollEvents))

(defn post-empty-event []
  (GLFW/glfwPostEmptyEvent))

; private? not documented in glfw
;(defn raw-mouse-motion-supported []
;  (GLFW/glfwRawMouseMotionSupported))

(defn request-window-attention [window]
  (GLFW/glfwRequestWindowAttention (long window)))

(defn restore-window [window]
  (GLFW/glfwRestoreWindow (long window)))

(def-callback-macro "set-char-callback" GLFW/glfwSetCharCallback org.lwjgl.glfw.GLFWCharCallbackI $window $unicode)

(def-callback-macro "set-char-mods-callback" GLFW/glfwSetCharModsCallback org.lwjgl.glfw.GLFWCharModsCallbackI $window $codepoint $mods)

(defn set-clipboard-string [window string]
  (GLFW/glfwSetClipboardString (long window) string))

(defn set-cursor [window cursor]
  (GLFW/glfwSetCursor (long window) (long cursor)))

(def-callback-macro "set-cursor-enter-callback" GLFW/glfwSetCursorEnterCallback org.lwjgl.glfw.GLFWCursorEnterCallbackI $window $entered)

(defn set-cursor-pos [window x y]
  (GLFW/glfwSetCursorPos (long window) (double x) (double y)))

(def-callback-macro "set-cursor-pos-callback" GLFW/glfwSetCursorPosCallback org.lwjgl.glfw.GLFWCursorPosCallbackI $window $xpos $ypos)

(def-callback-macro "set-drop-callback" GLFW/glfwSetDropCallback org.lwjgl.glfw.GLFWDropCallbackI $window $count $names)

(defn set-error-callback [callback]
  (GLFW/glfwSetErrorCallback 
    (reify org.lwjgl.glfw.GLFWErrorCallbackI
      (invoke [$this $error $description] callback))))

(def-callback-macro "set-framebuffer-size-callback" GLFW/glfwSetFramebufferSizeCallback org.lwjgl.glfw.GLFWFramebufferSizeCallbackI $window $width $height)

(defn set-gamma [monitor gamma]
  (GLFW/glfwSetGamma (long monitor) (float gamma)))

(defn set-gamma-ramp [monitor ramp]
  (GLFW/glfwSetGammaRamp (long monitor) ramp))

(defn set-input-mode [window mode value]
  (GLFW/glfwSetInputMode (long window) (int mode) (int value)))

(defn set-joystick-callback [callback]
  (GLFW/glfwSetJoystickCallback
    (reify org.lwjgl.glfw.GLFWJoystickCallbackI 
      (invoke [$this $jid $event] callback))))

(defn set-joystick-user-pointer [monitor pointer]
  (GLFW/glfwSetJoystickUserPointer (long monitor) (long pointer)))

(def-callback-macro "set-key-callback" GLFW/glfwSetKeyCallback org.lwjgl.glfw.GLFWKeyCallbackI $window $key $scancode $action $mods)

(defn set-monitor-callback [callback]
  (GLFW/glfwSetMonitorCallback 
    (reify org.lwjgl.glfw.GLFWMonitorCallbackI
      (invoke [$this $monitor $event] callback))))

(defn set-monitor-user-pointer [monitor pointer]
  (GLFW/glfwSetMonitorUserPointer (long monitor) (long pointer)))

(def-callback-macro "set-mouse-button-callback" GLFW/glfwSetMouseButtonCallback org.lwjgl.glfw.GLFWMouseButtonCallbackI $window $button $action $mods)

(def-callback-macro "set-scroll-callback" GLFW/glfwSetScrollCallback org.lwjgl.glfw.GLFWScrollCallbackI $window $xoffset $yoffset)

(defn set-time [t]
  (GLFW/glfwSetTime (double t)))

(defn set-window-aspect-ratio [window numer denom]
  (GLFW/glfwSetWindowAspectRatio (long window) (int numer) (int denom))) ; accept ratios???

(defn set-window-attrib [window attrib value]
  (GLFW/glfwSetWindowAttrib (long window) (int attrib) (int value)))

(def-callback-macro "set-window-close-callback" GLFW/glfwSetWindowCloseCallback org.lwjgl.glfw.GLFWWindowCloseCallbackI $window)

(def-callback-macro "set-window-content-scale-callback" GLFW/glfwSetWindowContentScaleCallback org.lwjgl.glfw.GLFWWindowContentScaleCallbackI $window $xscale $yscale)

(def-callback-macro "set-window-focus-callback" GLFW/glfwSetWindowFocusCallback org.lwjgl.glfw.GLFWWindowFocusCallbackI $window $focused)

(defn set-window-icon [window image]
  (GLFW/glfwSetWindowIcon (long window) image))

(def-callback-macro "set-window-iconify-callback" GLFW/glfwSetWindowIconifyCallback org.lwjgl.glfw.GLFWWindowIconifyCallbackI $window $iconified)

(def-callback-macro "set-window-maximize-callback" GLFW/glfwSetWindowMaximizeCallback org.lwjgl.glfw.GLFWWindowMaximizeCallbackI $window $maximized)

(defn set-window-monitor [window monitor x y w h rr]
  (GLFW/glfwSetWindowMonitor (long window) (long monitor) (int x) (int y) (int w) (int h) (int rr)))

(defn set-window-opacity [window opacity]
  (GLFW/glfwSetWindowOpacity (long window) (float opacity)))

(defn set-window-pos [window x y]
  (GLFW/glfwSetWindowPos (long window) (int x) (int y)))

(def-callback-macro "set-window-pos-callback" GLFW/glfwSetWindowPosCallback org.lwjgl.glfw.GLFWWindowPosCallbackI $window $xpos $ypos)

(def-callback-macro "set-window-refresh-callback" GLFW/glfwSetWindowRefreshCallback org.lwjgl.glfw.GLFWWindowRefreshCallbackI $window)

(defn set-window-should-close [window value]
  (GLFW/glfwSetWindowShouldClose (long window) (boolean value)))

(defn set-window-size [window w h]
  (GLFW/glfwSetWindowSize (long window) (int w) (int h)))

(def-callback-macro "set-window-size-callback" GLFW/glfwSetWindowSizeCallback org.lwjgl.glfw.GLFWWindowSizeCallbackI $window $width $height)

(defn set-window-size-limits [window nw nh xw xh]
  (GLFW/glfwSetWindowSizeLimits (long window) (int nw) (int nh) (int xw) (int xh)))

(defn set-window-title [window title]
  (GLFW/glfwSetWindowTitle (long window) title))

(defn set-window-user-pointer [window pointer]
  (GLFW/glfwSetWindowUserPointer (long window) (long pointer)))

(defn show-window [window]
  (GLFW/glfwShowWindow (long window)))

(defn swap-buffers [window]
  (GLFW/glfwSwapBuffers (long window)))

(defn swap-interval [interval]
  (GLFW/glfwSwapInterval (int interval)))

(defn terminate []
  (GLFW/glfwTerminate))

(defn update-gamepad-mappings [string]
  (GLFW/glfwUpdateGamepadMappings string))

(defn wait-events []
  (GLFW/glfwWaitEvents))

(defn wait-events-timeout [timeout]
  (GLFW/glfwWaitEventsTimeout (double timeout)))

(defn window-hint [hint value]
  (GLFW/glfwWindowHint (int hint) (int value)))

(defn window-hint-string [hint string]
  (GLFW/glfwWindowHintString (int hint) string))

(defn window-should-close [window]
  (GLFW/glfwWindowShouldClose (long window)))
