<img src="https://awoo.systems/trans_rights_badge.svg"/><br>
# liblesbian

A gay wrapper over lwjgl3 in Clojure for making games.

## Installation

Make sure Clojure >=1.9 and Leiningen are installed. Clone repo. Done.

## Usage

Provided run and repl scripts are bash calls to lein run and lein repl.

## Notes

* Project was created with Leiningen and has a standard structure, but this may change in the future if necessary.
* Many parts are missing. Even rendering is not done. This is pre-alpha code.
* What it can do: load a 3D file (blender, etc) and turn it into something opengl should be able to render, hopefully. Create a window to display it. Not sure if it can actually display it.
