(let [lwjgl-ver "3.2.3"]
  (defproject liblesbian "0.1.0-SNAPSHOT"
    :description "liblesbian: homosexual clojure for games"
    :url "http://example.com/FIXME"
    :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
              :url "https://www.eclipse.org/legal/epl-2.0/"}
    :dependencies [[org.clojure/clojure "1.10.0"]
                   [org.clojure/core.async "0.4.500"]
                   [tile-soup "0.3.1"]
                   [net.mikera/core.matrix "0.62.0"]
                   [net.mikera/vectorz-clj "0.48.0"]
                   [org.joml/joml "1.9.19"]
                   [org.lwjgl/lwjgl ~lwjgl-ver]
                   [org.lwjgl/lwjgl ~lwjgl-ver :classifier "natives-linux"]
                   [org.lwjgl/lwjgl-opengl ~lwjgl-ver]
                   [org.lwjgl/lwjgl-opengl ~lwjgl-ver :classifier "natives-linux"]
                   [org.lwjgl/lwjgl-glfw ~lwjgl-ver]
                   [org.lwjgl/lwjgl-glfw ~lwjgl-ver :classifier "natives-linux"]
                   [org.lwjgl/lwjgl-assimp ~lwjgl-ver]
                   [org.lwjgl/lwjgl-assimp ~lwjgl-ver :classifier "natives-linux"]]
    :main ^:skip-aot liblesbian.core
    :target-path "target/%s"
    :resources-path "resources"
    :profiles {:uberjar {:aot :all}}))
